import java.util.Scanner;

public class BilanganPrima {
    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        int i ;
        int num;
        String primeNumbers = "";
        System.out.println("Bilangan Prima yang akan ditampilkan : ");
        int n = inp.nextInt();
        inp.close();
        for (i = 1; i <= n; i++) {
            int counter=0;
            for (num = i; num >=1 ; num--) {
                if (i%num==0) {
                    counter = counter + 1;
                }
            }
            if (counter == 2) {
                primeNumbers = primeNumbers + i + " ";
            }
        }
        System.out.println(primeNumbers);
    }
}
