import java.util.Scanner;

public class Challenge1 {
    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        int menu = 0;
        do {
            menuUtama();
            while (!inp.hasNextInt()){
                System.out.println("Pilihan Bukan Angka");
                System.out.println("Silahkan Masukkan Angka");
                inp.next();
                System.out.println();
                menuUtama();
            }
            menu = inp.nextInt();
            System.out.println("-----------------------------------");
            if (menu == 1) {
                    int pilihan = 0;
                    do {
                        subMenu();
                        while (!inp.hasNextInt()){
                            System.out.println("Pilihan Bukan Angka");
                            System.out.println("Silahkan Masukkan Angka");
                            inp.next();
                            System.out.println();
                            subMenu();
                        }
                        pilihan = inp.nextInt();
                        System.out.println("-------------------------------");

                        switch (pilihan) {
                            case 1:
                                luasPersegi();
                                break;
                            case 2:
                                luasLingkaran();
                                break;
                            case 3:
                                luasSegitiga();
                                break;
                            case 4:
                                luasPersegiPanjang();
                                break;
                        }
                    } while (pilihan != 5);
                }
            if (menu == 2) {
                    int pilihan = 0;
                    do {
                        subMenuV();
                        while (!inp.hasNextInt()){
                            System.out.println("Pilihan Bukan Angka");
                            System.out.println("Silahkan Masukkan Angka");
                            inp.next();
                            System.out.println();
                            subMenuV();
                        }
                        pilihan = inp.nextInt();
                        System.out.println("-------------------------------");
                        switch (pilihan) {
                            case 1:
                                volKubus();
                                break;
                            case 2:
                                volBalok();
                                break;
                            case 3:
                                volTabung();
                                break;
                        }
                    } while (pilihan != 4);
                }
        } while (menu!=0);
        System.out.println("====Program Diakhiri======");
        System.out.println("      Terimakasih");
        System.exit(0);
    }
    private static void menuUtama(){
        System.out.println("-----------------------------------");
        System.out.println("Kalkulator Penghitung Luas & Volume");
        System.out.println("-----------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume");
        System.out.println("0. Tutup Aplikasi");
        System.out.println("-----------------------------------");
        System.out.print("Pilihan Anda [1/2/0] : ");
    }
    private static void subMenu(){
        System.out.println("-------------------------------");
        System.out.println("Pilih Bidang Yang Akan Dihitung");
        System.out.println("-------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("5. Kembali Ke Menu Sebelumnya");
        System.out.println("-------------------------------");
        System.out.print("Pilihan Anda [1/2/3/4/5] : ");
    }
    private static void subMenuV(){
        System.out.println("-------------------------------");
        System.out.println("Pilih Bidang Yang Akan Dihitung");
        System.out.println("-------------------------------");
        System.out.println("1. Volume Kubus");
        System.out.println("2. Volume Balok");
        System.out.println("3. Volume Tabung");
        System.out.println("4. Kembali Ke Menu Sebelumnya");
        System.out.println("-------------------------------");
        System.out.print("Pilihan Anda [1/2/3/4] : ");
    }

//    Method Luas Bangun
    public static void luasPersegi(){
        Scanner inp = new Scanner(System.in);

        System.out.println("----------------------------");
        System.out.println("Anda Memilih Persegi ");
        System.out.println("----------------------------");
        System.out.print("Masukkan Sisi\t: ");
        int sisi = inp.nextInt();
        Double lPersegi = Math.pow(sisi, 2);

        System.out.println();
        System.out.println("Processing...");
        System.out.println();
        System.out.println("Luas dari Persegi adalah "+lPersegi);
        PressAnyKey();
    }
    private static void luasLingkaran(){
        Scanner inp = new Scanner(System.in);
        double phi = 3.14;

        System.out.println("----------------------------");
        System.out.println("Anda Memilih Lingkaran ");
        System.out.println("----------------------------");
        System.out.print("Masukkan jari-jari\t: ");
        int jari = inp.nextInt();

        double lLingkaran = phi * Math.pow(jari,2);

        System.out.println();
        System.out.println("Processing...");
        System.out.println();
        System.out.println("Luas dari Lingkaran adalah "+lLingkaran);
        PressAnyKey();
    }
    private static void luasSegitiga(){
        Scanner inp = new Scanner(System.in);

        System.out.println("----------------------------");
        System.out.println("Anda Memilih Segitiga ");
        System.out.println("----------------------------");
        System.out.print("Masukkan Nilai Alas\t : ");
        int alas = inp.nextInt();
        System.out.print("Masukkan Nilai Tinggi\t :");
        int tinggi = inp.nextInt();

        double lSegitiga = 0.5*alas*tinggi;
        System.out.println();
        System.out.println("Processing...");
        System.out.println();
        System.out.println("Luas dari Segitiga adalah "+lSegitiga);
        PressAnyKey();
    }
    private static void luasPersegiPanjang(){
        Scanner inp = new Scanner(System.in);

        System.out.println("----------------------------");
        System.out.println("Anda Memilih Persegi Panjang ");
        System.out.println("----------------------------");
        System.out.print("Masukkan Nilai Panjang\t : ");
        int panjang = inp.nextInt();
        System.out.print("Masukkan Nilai Lebar\t :");
        int lebar = inp.nextInt();

        double lPPanjang = panjang*lebar;
        System.out.println();
        System.out.println("Processing...");
        System.out.println();
        System.out.println("Luas dari Segitiga adalah "+lPPanjang);
        PressAnyKey();
    }

//    Method Volume Bangun
    private static void volKubus(){
        Scanner inp = new Scanner(System.in);

        System.out.println("----------------------------");
        System.out.println("Anda Memilih Volume Kubus ");
        System.out.println("----------------------------");
        System.out.print("Masukkan Sisi\t : ");
        int sisi = inp.nextInt();

        double vKubus = Math.pow(sisi,3);
        System.out.println();
        System.out.print("Processing...");
        System.out.println();
        System.out.println("Volume dari Kubus adalah "+vKubus);
        PressAnyKey();
    }
    private static void volBalok(){
        Scanner inp = new Scanner(System.in);

        System.out.println("----------------------------");
        System.out.println("Anda Memilih Volume Balok ");
        System.out.println("----------------------------");
        System.out.print("Masukkan Panjang\t : ");
        int pjg = inp.nextInt();
        System.out.print("Masukkan Lebar\t : ");
        int lbr = inp.nextInt();
        System.out.print("Masukkan Tinggi\t : ");
        int t = inp.nextInt();

        int vBalok = pjg*lbr*t;
        System.out.println();
        System.out.println("Processing...");
        System.out.println();
        System.out.println("Volume dari Balok adalah "+vBalok);
        PressAnyKey();
    }
    private static void volTabung(){
        Scanner inp = new Scanner(System.in);
        double phi = 3.14;

        System.out.println("----------------------------");
        System.out.println("Anda Memilih Volume Tabung ");
        System.out.println("----------------------------");
        System.out.print("Masukkan Jari-jari\t : ");
        int jari = inp.nextInt();
        System.out.print("Masukkan Tinggi\t : ");
        int tinggi = inp.nextInt();

       double vTabung = phi*Math.pow(jari,2)*tinggi;
        System.out.println();
        System.out.println("Processing...");
        System.out.println();
        System.out.println("Volume dari vTabung adalah "+vTabung);
        PressAnyKey();
    }

    public static void PressAnyKey() {
        Scanner inp = new Scanner(System.in);

        System.out.print("Tekan apa saja untuk kembali ke menu utama");
        char press = inp.next().charAt(0);

        if (press>='a' && press<='z'){
            main(null);
        } else {
            main(null);
        }
//        System.out.print("Tekan Enter untuk kembali ke menu utama");
//        try
//        {
//            System.in.read();
//        }
//        catch(Exception e)
//        {}
    }
}
